/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

// Network topology
//
// Packets sent to the device "tap0" on the Linux host will be sent to the
// tap bridge on node zero and then emitted onto the ns-3 simulated CSMA
// network.  ARP will be used on the CSMA network to resolve MAC addresses.
// Packets destined for the CSMA device on node zero will be sent to the
// device "tap0" on the linux Host. Same for tap 1.                                                     
//                                                                   
//  +-----------+                           +-----------+                                           
//  |  external |                           |  external |                                              
//  |raspberryPi|                           |raspberryPi|                                                   
//  |_____0_____|                           |_____1_____|                                                    
//  |   eth0    |                           |   eth0    |                                                                                  
//  +-----------+                           +-----------+                                                       
//       |                                        |
//  +-----------+                           +-----------+
//  | Interface |                           | Interface |
//  |  Ubuntu   |                           |  Ubuntu   |
//  |"enp4s0f0" |                           |"enp4s0f0" |
//  +-----------+                           +-----------+                                               
//       |                                       |
//  +----------+                            +----------+
//  |  Bridge  |                            |  Bridge  |
//  |  "br0"   |                            |  "br1"   |
//  +----------+                            +----------+                                              
//       |                                       |
//  +----------+                            +----------+
//  |Tap-Device|                            |Tap-Device|
//  |  "tap0"  |                            |  "tap1"  |
//  +----------+                            +----------+                                                       
//       |           n0            n1            |
//       |       +--------+    +--------+        |
//       +-------|  tap   |    |  tap   |--------+
//               | bridge |    | bridge |
//               +--------+    +--------+    
//               |  CSMA  |    |  CSMA  |
//               +--------+    +--------+
//                   |             |           
//                   |             |             
//                   |             |             
//                   =================
//                                 CSMA LAN 10.1.1
//
// The CSMA device on node zero is a ghost node which means it doesn't need an IP-address
// The CSMA device on node one is a ghost node which means it doesn't need an IP-address
//
// Some simple things to do:
//
// 1) Ping one of the simulated nodes
//
//    ./waf --run tap-csma&
//    ping 10.1.1.2
//
#include <iostream>
#include <fstream>

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/wifi-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/tap-bridge-module.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("TapCsmaModified");

int 
main (int argc, char *argv[])
{
  std::string mode = "UseBridge";
  std::string tapName01 = "tap0";
  std::string tapName02 = "tap1";

  CommandLine cmd (__FILE__);
  //cmd.AddValue ("mode", "Mode setting of TapBridge", mode);
  //cmd.AddValue ("tapName", "Name of the OS tap device", tapName);
  cmd.Parse (argc, argv);

  GlobalValue::Bind ("SimulatorImplementationType", StringValue ("ns3::RealtimeSimulatorImpl"));
  GlobalValue::Bind ("ChecksumEnabled", BooleanValue (true));

  NodeContainer nodes;
  nodes.Create (2);

  CsmaHelper csma;
  csma.SetChannelAttribute ("DataRate", DataRateValue (5000000));
  csma.SetChannelAttribute ("Delay", TimeValue (MilliSeconds (2)));

  NetDeviceContainer devices = csma.Install (nodes);


  // Removed when devices with IP-addresses were removed
  // InternetStackHelper stack;
  // stack.Install (nodes);

  // Ipv4AddressHelper addresses;
  // addresses.SetBase ("10.1.1.0", "255.255.255.0", "0.0.0.3");
  // Ipv4InterfaceContainer interfaces = addresses.Assign (devices);

  //Install the ghost nodes for traffic tapping
  TapBridgeHelper tapBridge;
  tapBridge.SetAttribute ("Mode", StringValue (mode));
  tapBridge.SetAttribute ("DeviceName", StringValue (tapName01));
  tapBridge.Install (nodes.Get (0), devices.Get (0));
  tapBridge.SetAttribute ("DeviceName", StringValue (tapName02));
  tapBridge.Install (nodes.Get (1), devices.Get (1));

  // Connect other side to tap bridge. Node 3 to connect
  // tapBridgevirt.SetAttribute ("Mode", StringValue ("UseBridge"));
  // tapBridge.SetAttribute ("DeviceName", StringValue (tapVirtName));
  // tapBridge.Install (nodes.Get (3), devices.Get (3));
  

  csma.EnablePcapAll ("tap-csma", true);
  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

  Simulator::Stop (Minutes (10.));
  Simulator::Run ();
  Simulator::Destroy ();
}
